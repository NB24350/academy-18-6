package logica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ExpenseCalc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Date myDate = parseDate("2020-02-14");
		
		Date myDate2 = parseDate("2020-02-14");
		
		double extraShopping = 50;
		double extraOther = 50;
		
		double refValue = 50;
		double breakfeastperc = 100;
		double lunchbreakfestperc  = 100;
		double dinnerperc = 100;
		double quantity = 1;
		
		
		
		int aux = ExpenseCalc.dateToDays(myDate, myDate2);
		
		System.out.println(aux);
		
		ExpenseCalc expense = new ExpenseCalc();
		System.out.println(expense.calculateExpenses(ExpenseType.FOOD, refValue, 0, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2));
		System.out.println(expense.calculateExpenses(ExpenseType.SHOPPING, refValue, extraShopping, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2));
		System.out.println(ExpenseCalc.calculateExpenses(ExpenseType.ACCOMODATION, refValue, 0, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2));
		System.out.println(expense.calculateExpenses(ExpenseType.TRANSPORTATION, refValue, 0, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2));
		System.out.println(expense.calculateExpenses(ExpenseType.OTHER, refValue, extraOther, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2));
		System.out.println(expense.calculateExpenses(ExpenseType.LEISURE, refValue, 0, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2));
		
		double total = calculateAllExpenses(refValue, extraShopping, extraOther, breakfeastperc, lunchbreakfestperc, dinnerperc, quantity, myDate, myDate2);
		
		System.out.println(total);
		
		
		

	}
	
	/*
    *
    * Nota: extraValue é o valor de extra para todas as pessoas e não para cada uma.
    *
    *
    * */
   private static double calculateExpenses(ExpenseType type, double refValue, double extraValue, double breakfestPerc , double lunchPerc, double dinnerPerc, double quantity, Date d1, Date d2){

       double value;
       float days = ExpenseCalc.dateToDays(d1,d2);
       /*
       *   Atribuir valores com o getters
       *
       ExpenseType type;
       double refValue;
       double extraValue;
       double breakfestPerc;
       double lunchPerc;
       double dinnerPerc;
       double quantity;
       Date d1;
       Date d2;
       */

       switch (type) {
           case FOOD:
               double breakValue = refValue * (breakfestPerc / 100);
               double lunchValue = refValue * (lunchPerc / 100);
               double dinnerValue = refValue * (dinnerPerc / 100);
               value = (breakValue + lunchValue + dinnerValue) * quantity * days;
               break;
           case ACCOMODATION:
               value = refValue * quantity;
               break;
           case SHOPPING:
               double shopValue = refValue * (extraValue / 100);
               value = shopValue * quantity;
               break;
           case TRANSPORTATION:
               value = refValue * quantity;
               break;
           case LEISURE:
               value = refValue * quantity;
               break;
           default:
               double otherValue = refValue * (extraValue / 100);
               value = otherValue * quantity;

       }
       return value;
   }


   private static int dateToDays(Date date1, Date date2){
       long diff = date2.getTime() - date1.getTime();
       if(diff == 0) {
    	   return 1;
       }
       int days = (int) (diff / (1000*60*60*24));
       return days;
   }
   
   private static double calculateAllExpenses(double refValue, double extraValueShopping, double extraValueOthers, double breakfestPerc , double lunchPerc, double dinnerPerc, double quantity, Date d1, Date d2) {
	   double total = 0.0;
	   double extraValue = 0.0;
	   ExpenseType [] types = ExpenseType.values();
	   
	   for (ExpenseType type : types) {
		   switch (type) {
		   case SHOPPING:
			    extraValue = extraValueShopping;
			    break;
		   case OTHER:
			   extraValue = extraValueOthers;
			   break;
		   default:
			   extraValue = 0;
		   }
	   total += ExpenseCalc.calculateExpenses(type, refValue,  extraValue,  breakfestPerc ,  lunchPerc,  dinnerPerc,  quantity, d1,  d2 );
	}
	   return total;
   }
   
   public static Date parseDate(String date) {
	     try {
	         return new SimpleDateFormat("yyyy-MM-dd").parse(date);
	     } catch (ParseException e) {
	         return null;
	     }
	  }
}

