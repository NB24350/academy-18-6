package com.nb.academy.academy_18_06.models;

import com.nb.academy.academy_18_06.enums.CityEnum;
import com.nb.academy.academy_18_06.enums.CountryEnum;
import com.nb.academy.academy_18_06.enums.TypeExpenseEnum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Destination {
    private CityEnum city;
    private CountryEnum country;
    private Date startDate;
    private Date endDate;
    private int numberOfPeople;
    private List<Expense> expenses;


    public Destination(CityEnum city, CountryEnum country, Date startDate, Date endDate, int numberOfPeople) {
        this.city = city;
        this.country = country;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numberOfPeople = numberOfPeople;
        this.expenses = new ArrayList<Expense>();
        Expense expense = new Expense(TypeExpenseEnum.ACCOMODATION, "", 0, null);
        expenses.add(expense);
    }
}
