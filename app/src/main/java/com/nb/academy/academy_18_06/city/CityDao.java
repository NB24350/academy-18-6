/*
package com.nb.academy.academy_18_06.City;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public class CityDao {

    @Query("SELECT * FROM Country WHERE id = :id")
    LiveData<Country> getCountry(int id);


    @Query("SELECT * FROM Country")
    LiveData<List<Country>> getCountry();


    @Query("SELECT * FROM City WHERE countryId = :countryId")
    LiveData<List<City>> getCities(int countryId);


    @Insert(onConflict = REPLACE)
    void saveCountry(Country country);


    @Insert(onConflict = REPLACE)
    void save(List<Country> states);


    @Insert(onConflict = REPLACE)
    void saveCities(List<City> cities);

}

*/
