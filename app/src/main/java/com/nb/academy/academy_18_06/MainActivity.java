package com.nb.academy.academy_18_06;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.nb.academy.academy_18_06.database.AppDatabase;
import com.nb.academy.academy_18_06.trip.Trip;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    /*
     *
     * Nota: extraValue é o valor de extra para todas as pessoas e não para cada uma.
     * */
    public double calculateExpenses(T type, double refValue, double extraValue, double breakfestPerc , double lunchPerc, double dinnerPerc, int numberPersons, int nrDays){

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        final AppDatabase dataBase = Room.databaseBuilder(getApplicationContext(),
                                                    AppDatabase.class, "App_db")
                                                    .fallbackToDestructiveMigration()
                                                    .allowMainThreadQueries()
                                                    .build();
        /*dataBase.clearAllTables();
        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        }).start();*/

        Trip trip = new Trip();
        trip.setBudget(100);
        trip.setOriginCity("London");
        trip.setOriginCountry("UK");
        trip.setName("Finalistas");
        dataBase.tripDAO().insertTrip(trip);
        List<Trip> tripList = dataBase.tripDAO().getAllTrips();
        Log.i("TEST", ""+tripList.get(0).getBudget());
        Log.i("TEST", ""+tripList.get(0).getOriginCity());
        Log.i("TEST", ""+tripList.get(0).getOriginCountry());
        Log.i("TEST", ""+tripList.get(0).getName());



    }

        double value;

        if ( type.equals(T.FOOD)){

            double breakValue = refValue*(breakfestPerc/100);
            double lunchValue = refValue*(lunchPerc/100);
            double dinnerValue = refValue*(dinnerPerc/100);
            value = (breakValue + lunchValue + dinnerValue) * numberPersons * nrDays;

        } else if (type.equals(T.ACCOMODATION)){

            value = refValue * numberPersons;

        } else {
            value = extraValue;
        }
        System.out.println(value);
        return value;
    }
}
