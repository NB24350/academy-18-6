package com.nb.academy.academy_18_06.user;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nb.academy.academy_18_06.trip.Trip;

import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {

    @Insert
    void insertUser(User user);

    @Insert
    void insertMultipleUsers(List<User> userList);

    @Query("SELECT * FROM USER WHERE id = :id")
    User getUserById(int id);

    @Query("SELECT * FROM USER WHERE name = :userName")
    User getUserByName(String userName);

    @Query("SELECT * FROM USER")
    List<User> getAllUsers();

    @Update
    void updateTrip(User user);

    @Delete
    void deleteTrip(User user);

    @Query("DELETE FROM USER")
    void deleteAll();
}

