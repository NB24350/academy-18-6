package com.nb.academy.academy_18_06.country;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface CountryDao {

    @Insert(onConflict = REPLACE)
    void insert(Country country);

    @Insert(onConflict = REPLACE)
    void insertMultipleCountries(List<Country> countryList);

    @Update
    void updateCountry(Country country);

    @Query("SELECT * FROM COUNTRY WHERE id = :id")
    Country getCountryId(int id);

    @Query("SELECT * FROM COUNTRY ORDER BY name ASC")
    List<Country> getAllCountries();

    @Delete
    void deleteCountry(Country country);

    @Query("DELETE FROM COUNTRY")
    void deleteAll();
}
