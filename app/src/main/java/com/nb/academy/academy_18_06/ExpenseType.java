package com.nb.academy.academy_18_06;

public enum ExpenseType {
    FOOD,
    ACCOMODATION,
    TRANSPORTATION,
    SHOPPING,
    LEISURE,
    OTHER
}

