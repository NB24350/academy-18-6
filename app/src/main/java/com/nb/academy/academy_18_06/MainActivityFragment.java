package com.nb.academy.academy_18_06;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    /*
     *
     * Nota: extraValue é o valor de extra para todas as pessoas e não para cada uma.
     * */
    public double calculateExpenses(ExpenseType type, double refValue, double extraValue, double breakfestPerc , double lunchPerc, double dinnerPerc, int numberPersons, int nrDays){


        double value;

        if ( type.equals(type.FOOD)){

            double breakValue = refValue*(breakfestPerc/100);
            double lunchValue = refValue*(lunchPerc/100);
            double dinnerValue = refValue*(dinnerPerc/100);
            value = (breakValue + lunchValue + dinnerValue) * numberPersons * nrDays;

        } else if (type.equals(type.ACCOMODATION)){

            value = refValue * numberPersons;

        } else {
            value = extraValue;
        }
        System.out.println(value);
        return value;
    }
}