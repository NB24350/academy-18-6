package com.nb.academy.academy_18_06.expense;

public enum ExpenseType {
    FOOD,
    ACCOMMODATION,
    LEISURE,
    TRANSPORTATION,
    SHOPPING,
    OTHER
}
