package com.nb.academy.academy_18_06.models;

import com.nb.academy.academy_18_06.enums.TypeExpenseEnum;

import java.util.Date;

public class Expense {

    private TypeExpenseEnum typeExpense;
    private String name;
    private int budgetValue;
    private int valueSpend;
    private String comments;
    private Date date;

    public Expense(TypeExpenseEnum typeExpense, String name, int budgetValue, Date date) {
        this.typeExpense = typeExpense;
        this.name = name;
        this.budgetValue = budgetValue;
        this.date = date;
        this.valueSpend = budgetValue;
    }

    public TypeExpenseEnum getTypeExpense() {
        return typeExpense;
    }

    public void setTypeExpense(TypeExpenseEnum typeExpense) {
        this.typeExpense = typeExpense;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBudgetValue() {
        return budgetValue;
    }

    public void setBudgetValue(int budgetValue) {
        this.budgetValue = budgetValue;
    }

    public int getValueSpend() {
        return valueSpend;
    }

    public void setValueSpend(int valueSpend) {
        this.valueSpend = valueSpend;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
