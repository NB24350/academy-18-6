package com.nb.academy.academy_18_06;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class ViagensRecyclers extends RecyclerView.Adapter<ViagensRecyclers.MyViewHolder>{

    private List<Viagens> viagens;
    int minteger = 0;
    EditText destino;
    TextView dataInicio;
    EditText dataInicioCampo;
    TextView dataFim;
    EditText dataFimCampo;
    TextView nPassageiros;
    Button decrease;
    Button increase;

    public ViagensRecyclers(List<Viagens> viagens) {
        this.viagens = viagens;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viagensList = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_viagem, parent,false);
        destino = (EditText) viagensList.findViewById(R.id.campoDestino);
        dataInicio = (TextView) viagensList.findViewById(R.id.dataInicio);
        dataInicioCampo = (EditText) viagensList.findViewById(R.id.dataInicioCampo);
        dataFim = (TextView) viagensList.findViewById(R.id.dataFim);
        dataFimCampo = (EditText) viagensList.findViewById(R.id.dataFimCampo);
        nPassageiros = (TextView) viagensList.findViewById(R.id.nPassageiros);
        return new MyViewHolder(viagensList);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return viagens.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }


    public void increaseInteger(View view) {

        minteger = minteger + 1;
    }
    public void decreaseInteger(View view) {

        minteger = minteger - 1;
    }

    public void mostra(){
    }
}
