package com.nb.academy.academy_18_06.trip;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.arch.persistence.room.Entity;

import com.nb.academy.academy_18_06.destination.Destination;


@Entity(tableName = "TRIP")
public class Trip {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int id;

    @ColumnInfo(name = "BUDGET")
    private int budget;

    @ColumnInfo(name = "ORIGIN_COUNTRY")
    private String originCountry;

    @ColumnInfo(name = "ORIGIN_CITY")
    private String originCity;

    @ColumnInfo(name = "NAME")
    private String name;

    @ColumnInfo(name = "DESTINATION")
    private String destination;

    public Trip(){}

    @Ignore
    public Trip(int id, int budget, String country, String city, String name, String destination){
        this.id = id;
        this.budget = budget;
        this.originCountry = country;
        this.originCity = city;
        this.name = name;
        this.destination = destination;
    }

    //GETTERS
    @NonNull
    public int getId(){
        return id;
    }

    @NonNull
    public int getBudget() { return budget; }

    @NonNull
    public String getOriginCountry(){
        return originCountry;
    }

    @NonNull
    public String getOriginCity(){
        return originCity;
    }

    @NonNull
    public String getName(){
        return name;
    }

    @NonNull
    public String getDestination(){
        return destination;
    }

    //SETTERS
    public void setId(@NonNull int id){ this.id = id; }

    public void setBudget(@NonNull int budget) { this.budget = budget; }

    public void setOriginCountry(@NonNull String country){
        this.originCountry = country;
    }

    public void setOriginCity(@NonNull String city){ this.originCity = city; }

    public void setName(@NonNull String name){
        this.name = name;
    }

    public void setDestination(@NonNull String destination){
        this.destination = destination;
    }
}
