package com.nb.academy.academy_18_06.destination;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "DESTINATIONS")
public class Destination {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="DESTINATION_ID")
    int id_dest;

    @NonNull
    @ColumnInfo(name="TRIP_ID")
    int id_trip;

    @NonNull
    @ColumnInfo(name="COUNTRY")
    String country;

    @NonNull
    @ColumnInfo(name="CITY")
    String city;

    @NonNull
    @ColumnInfo(name="START_DATE")
    String startDate;

    @NonNull
    @ColumnInfo(name="END_DATE")
    String endDate;

    @NonNull
    @ColumnInfo(name="NUMBER_OF_TRAVELERS")
    int n_people;

    @NonNull
    @ColumnInfo(name="BREAKFAST_PERCENTAGE")
    float breakfast;

    @NonNull
    @ColumnInfo(name="LUNCH_PERCENTAGE")
    float lunch;

    @NonNull
    @ColumnInfo(name="DINNER_PERCENTAGE")
    float dinner;

    public Destination(){}

    @NonNull
    public int getId_dest() {
        return id_dest;
    }

    public void setId_dest(@NonNull int id_dest) {
        this.id_dest = id_dest;
    }

    @NonNull
    public int getId_trip() {
        return id_trip;
    }

    public void setId_trip(@NonNull int id_trip) {
        this.id_trip = id_trip;
    }

    @NonNull
    public String getCountry() { return country; }

    public void setCountry(@NonNull String countryName) {
        this.country = countryName;
    }

    @NonNull
    public String getCity() {
        return city;
    }

    public void setCity(@NonNull String cityName) {
        this.city = cityName;
    }

    @NonNull
    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(@NonNull String startDate) { this.startDate = startDate; }

    @NonNull
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(@NonNull String endDate) {
        this.endDate = endDate;
    }

    @NonNull
    public int getPeople() {
        return n_people;
    }

    public void setPeople(@NonNull int n_people) {
        this.n_people = n_people;
    }

    @NonNull
    public float getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(@NonNull float breakfast) {
        this.breakfast = breakfast;
    }

    @NonNull
    public float getLunch() {
        return lunch;
    }

    public void setLunch(@NonNull float lunch) {
        this.lunch = lunch;
    }

    @NonNull
    public float getDinner() {
        return dinner;
    }

    public void setDinner(@NonNull float dinner) {
        this.dinner = dinner;
    }
}
