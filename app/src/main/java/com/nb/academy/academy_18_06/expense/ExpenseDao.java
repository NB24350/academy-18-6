package com.nb.academy.academy_18_06.expense;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nb.academy.academy_18_06.destination.Destination;
import com.nb.academy.academy_18_06.expense.Expense;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ExpenseDao {

    @Insert(onConflict = REPLACE)
    void insertExpense(Expense expense);

    @Insert(onConflict = REPLACE)
    void insertAll(List<Expense> expenses);

    @Query("SELECT * FROM EXPENSE")
    List<Expense> getAllExpenses();

    @Query("SELECT * FROM EXPENSE WHERE id = :id")
    Expense getExpenseById(int id);

    @Update
    public void updateExpense(Expense expense);

    @Delete
    void delete(Expense expense);

    @Query("DELETE FROM EXPENSE")
    void deleteAll();
}
