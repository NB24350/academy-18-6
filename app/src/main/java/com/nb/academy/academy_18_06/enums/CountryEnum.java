package com.nb.academy.academy_18_06.enums;

public enum CountryEnum {

    PORTUGAL("Portugal"),
    NONE("None");

    private String name;

    CountryEnum(String name) {

        this.name = name;
    }
}
