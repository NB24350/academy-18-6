package com.nb.academy.academy_18_06;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExpenseCalc {

    /*
     *
     * Nota: extraValue é o valor extra para o agregado total e não para cada pessoa.
     *
     * No caso da despesa ser 'FOOD', é possível pedir o valor:
     * do almoço -> "Lunch"
     * do jantar -> "Dinner"
     * de ambos -> "All", *é utilizado no 'calculateAllExpenses(...)'
     *
     * */
    private static double calculateExpenses(ExpenseType type,String meal, double refValue, double extraValue, double breakfestPerc , double lunchPerc, double dinnerPerc, double quantity, Date d1, Date d2){

        double value;
        float days = ExpenseCalc.dateToDays(d1,d2);


        switch (type) {
            case FOOD:
                double mealValue = 0.0;
                if(meal.equals("Lunch")) {
                     mealValue = refValue * (lunchPerc / 100);
                }else if(meal.equals("Dinner")) {
                     mealValue = refValue * (dinnerPerc / 100);
                }else if(meal.equals("All")){
                    mealValue += (refValue * (lunchPerc / 100)) + (refValue * (dinnerPerc / 100));
                }
                value = mealValue * quantity * days;
                break;
            case ACCOMODATION:
                value = refValue * quantity;
                break;
            case SHOPPING:
                double shopValue = refValue * (extraValue / 100);
                value = shopValue * quantity;
                break;
            case TRANSPORTATION:
                value = refValue * quantity;
                break;
            case LEISURE:
                value = refValue * quantity;
                break;
            default: //OTHER
                double otherValue = refValue * (extraValue / 100);
                value = otherValue * quantity;
        }
        return value;
    }


    private static int dateToDays(Date date1, Date date2){
        long diff = date2.getTime() - date1.getTime();
        if(diff == 0) {
            return 1;
        }
        int days = (int) (diff / (1000*60*60*24));
        return days;
    }

    private static double calculateAllExpenses(double refValue, double extraValueShopping, double extraValueOthers, double breakfestPerc , double lunchPerc, double dinnerPerc, double quantity, Date d1, Date d2) {
        double total = 0.0;
        double extraValue = 0.0;
        ExpenseType [] types = ExpenseType.values();

        for (ExpenseType type : types) {
            switch (type) {
                case SHOPPING:
                    extraValue = extraValueShopping;
                    break;
                case OTHER:
                    extraValue = extraValueOthers;
                    break;
                default:
                    extraValue = 0;
            }
            total += ExpenseCalc.calculateExpenses(type,"All", refValue,  extraValue,  breakfestPerc ,  lunchPerc,  dinnerPerc,  quantity, d1,  d2 );
        }
        return total;
    }

    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }


}
