package com.nb.academy.academy_18_06.country;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "COUNTRY")
public class Country {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "ID")
    private int id;

    @ColumnInfo(name = "NAME")
    private String name;

    public Country(){ }

    @Ignore
    public Country(String name) {
        this.name = name;
    }

    @NonNull
    public int getId() { return id; }

    @NonNull
    public String getName() {
        return this.name;
    }

    public void setName(@NonNull String name){
        this.name = name;
    }

    public void setId(@NonNull int id) { this.id = id; }

}
