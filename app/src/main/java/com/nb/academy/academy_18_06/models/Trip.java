package com.nb.academy.academy_18_06.models;

import com.nb.academy.academy_18_06.enums.CityEnum;
import com.nb.academy.academy_18_06.enums.CountryEnum;

import java.util.ArrayList;
import java.util.List;

public class Trip {

    private String name;
    private CityEnum city;
    private CountryEnum country;
    private List<Destination> destinations;

    public Trip(String name, CityEnum city, CountryEnum country) {
        this.name = name;
        this.city = city;
        this.country = country;
        this.destinations = new ArrayList<Destination>();
        Destination destination = new Destination(CityEnum.None, CountryEnum.NONE,null,null,1);
        destinations.add(destination);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CityEnum getCity() {
        return city;
    }

    public void setCity(CityEnum city) {
        this.city = city;
    }

    public CountryEnum getCountry() {
        return country;
    }

    public void setCountry(CountryEnum country) {
        this.country = country;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
}
