package com.nb.academy.academy_18_06.enums;

public enum TypeExpenseEnum {

    ACCOMODATION("Accomodation"),
    FOOD("Food"),
    OTHER("Other");

    private String name;

    TypeExpenseEnum(String name) {
        this.name = name;
    }
}
