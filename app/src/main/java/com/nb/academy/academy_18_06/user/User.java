package com.nb.academy.academy_18_06.user;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "USER")
public class User {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int id;


    @ColumnInfo(name = "NAME")
    private String name;

    public User(@NonNull String name) {this.name = name;}

    //GETTERS
    @NonNull
    public int getId() {
        return id;
    }

    @NonNull
    public String getName(){
        return this.name;
    }

    //SETTERS
    public void setId(@NonNull int id) { this.id = id; }

    public void setName(@NonNull String name) {
        this.name = name;
    }
}
