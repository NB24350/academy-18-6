package com.nb.academy.academy_18_06;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class Main3Activity extends AppCompatActivity {


    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        btn = findViewById(R.id.btnResult);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    /*
     *
     * Nota: extraValue é o valor de extra para todas as pessoas e não para cada uma.
     * */
    private double calculateExpenses(ExpenseType type, double refValue, double extraValue, double breakfestPerc , double lunchPerc, double dinnerPerc, double quantity, Date d1, Date d2){

        double value;
        float days = this.dateToDays(d1,d2);
        /*
        ExpenseType type;
        double refValue;
        double extraValue;
        double breakfestPerc;
        double lunchPerc;
        double dinnerPerc;
        double quantity;
        Date d1;
        Date d2;
        */

        switch (type) {
            case FOOD:
                double breakValue = refValue * (breakfestPerc / 100);
                double lunchValue = refValue * (lunchPerc / 100);
                double dinnerValue = refValue * (dinnerPerc / 100);
                value = (breakValue + lunchValue + dinnerValue) * quantity * days;
                break;
            case ACCOMODATION:
                value = refValue * quantity;
                break;
            case SHOPPING:
                double shopValue = refValue * (extraValue / 100);
                value = shopValue * quantity;
                break;
            case TRANSPORTATION:
                value = refValue * quantity;
                break;
            case LEISURE:
                value = refValue * quantity;
                break;
            default:
                double otherValue = refValue * (extraValue / 100);
                value = otherValue * quantity;

        }
        return value;
    }


    private float dateToDays(Date date1, Date date2){
        long diff = date2.getTime() - date1.getTime();
        float days = (diff / (1000*60*60*24));
        return days;

    }
}
