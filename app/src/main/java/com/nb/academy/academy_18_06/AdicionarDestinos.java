package com.nb.academy.academy_18_06;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AdicionarDestinos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_destinos);
    }

    public void seguinte(View view){
        Intent intent = new Intent(getApplicationContext(), Plano.class);
        startActivity(intent);

    }

    public void guardar(View view) {

    }

    public void voltar(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
