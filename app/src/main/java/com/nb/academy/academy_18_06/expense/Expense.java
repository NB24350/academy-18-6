package com.nb.academy.academy_18_06.expense;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;
@Entity(tableName = "EXPENSE")
public class Expense {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "ID")
    private int id;

    @ColumnInfo(name = "NAME")
    private String name;

    @ColumnInfo(name = "DESCRIPTION")
    private String description;

    @ColumnInfo(name = "DATE")
    private String date;

    @ColumnInfo(name = "DESTINATION")
    private int destination;

    @ColumnInfo(name = "TYPE")
    private String type;

    //GETTERS
    @NonNull
    public int getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    @NonNull
    public int getDestination() {
        return destination;
    }

    @NonNull
    public String getType() {
        return type;
    }

    //SETTERS
    public void setId(@NonNull int id) {
        this.id = id;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setDescription(@NonNull String description) { this.description = description; }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    public void setDestination(@NonNull int destination) { this.destination = destination; }

    public void setType(@NonNull String type) { this.type = type; }
}
