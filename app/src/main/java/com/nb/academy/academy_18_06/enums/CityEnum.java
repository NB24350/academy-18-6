package com.nb.academy.academy_18_06.enums;

public enum CityEnum {

    PORTO("Porto"),
    None("None");

    private String name;

    CityEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
