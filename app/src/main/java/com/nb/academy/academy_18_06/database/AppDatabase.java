package com.nb.academy.academy_18_06.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.nb.academy.academy_18_06.city.City;
import com.nb.academy.academy_18_06.city.CityDao;
import com.nb.academy.academy_18_06.country.Country;
import com.nb.academy.academy_18_06.country.CountryDao;
import com.nb.academy.academy_18_06.destination.Destination;
import com.nb.academy.academy_18_06.destination.DestinationDao;
import com.nb.academy.academy_18_06.expense.ExpenseDao;
import com.nb.academy.academy_18_06.expense.Expense;
import com.nb.academy.academy_18_06.trip.Trip;
import com.nb.academy.academy_18_06.trip.TripDao;
import com.nb.academy.academy_18_06.user.User;
import com.nb.academy.academy_18_06.user.UserDao;

@Database(entities = {City.class, Country.class, Destination.class, Expense.class, Trip.class, User.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ExpenseDao expenseDao();

    public abstract CityDao cityDAO();
    public abstract CountryDao countryDAO();
    public abstract DestinationDao destinationDAO();
    public abstract TripDao tripDAO();
    public abstract UserDao userDAO();
}
