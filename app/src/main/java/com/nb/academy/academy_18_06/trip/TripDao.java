package com.nb.academy.academy_18_06.trip;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TripDao {

    @Insert
    void insertTrip(Trip trip);

    @Insert
    void insertMultipleTrips(List<Trip> tripList);

    @Query("SELECT * FROM TRIP")
    List<Trip> getAllTrips();

    @Query("SELECT * FROM TRIP WHERE id = :tripId")
    Trip getTripById(int tripId);

    @Update
    void updateTrip(Trip trip);

    @Delete
    void deleteTrip(Trip trip);

    @Query("DELETE FROM TRIP")
    void deleteAll();
}
