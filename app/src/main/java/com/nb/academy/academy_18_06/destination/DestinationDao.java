package com.nb.academy.academy_18_06.destination;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nb.academy.academy_18_06.country.Country;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface DestinationDao {

    @Insert(onConflict = REPLACE)
    public void insertDestination(Destination destination);

    @Insert(onConflict = REPLACE)
    public void insertMultipleDestinations(List<Destination> destinationList);

    @Query("SELECT * from DESTINATIONS WHERE DESTINATION_ID = :id")
    public Destination getDestinationById(int id);

    @Query("SELECT * FROM DESTINATIONS ORDER BY DESTINATION_ID ASC")
    List<Destination> getAllDestination();

    @Update
    public void updateDestination(Destination destination);

    @Delete
    public void deleteDestination(Destination destination);

    @Query("DELETE FROM DESTINATIONS")
    void deleteAll();
}
